package ru.digitalplant.analog.integration.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRole;

import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountAPIControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getAccountById_Success() throws Exception {

        DtoRole expectedRole = new DtoRole()
                .id(UUID.fromString("F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6"))
                .name("Пользователь")
                .isAdmin(false);

        DtoAccount expectedAccount = new DtoAccount()
                .id(UUID.fromString("a8d522eb-30f1-41c3-89e9-fdc6050f16aa"))
                .name("user2")
                .fio("Петров П.П.")
                .description("оператор")
                .isDelete(false);


        MvcResult result = mockMvc
                .perform(get("/accounts/{accountId}", expectedAccount.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expectedAccount.getName()))
                .andExpect(jsonPath("$.fio").value(expectedAccount.getFio()))
                .andExpect(jsonPath("$.description").value(expectedAccount.getDescription()))
                .andExpect(jsonPath("$.isDelete").value(expectedAccount.getIsDelete()))
                .andExpect(jsonPath("$.role.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.role.isAdmin").value(expectedRole.getIsAdmin()))
                //.andExpect(jsonPath("$.role.id").value(expectedRole.getId()))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getAccountById_NotFound() throws Exception {

        MvcResult result = mockMvc
                .perform(get("/accounts/{accountId}", UUID.fromString("00000000-0000-0000-0000-000000000000")))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getAccountById_BadRequest() throws Exception {

        MvcResult result = mockMvc
                .perform(get("/accounts/sdsdsdsdsds"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    @Ignore
    @Sql(scripts = "classpath:db/test-data.sql")
    public void addAccount_Success() throws Exception {

        UUID accountId = UUID.randomUUID();
        UUID roleId = UUID.fromString("F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6");

        DtoRole expectedRole = new DtoRole()
                .id(roleId)
                .name("Пользователь")
                .isAdmin(false);


        DtoAccount expectedAccount = new DtoAccount()
                .id(accountId)
                .name("userGenerated")
                .fio("Серов С.С.")
                .description("Сгенериророванный тестом пользователь")
                .role(expectedRole)
                .isDelete(false);

        MvcResult result = mockMvc
                .perform(post("/accounts")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expectedAccount)))
                .andExpect(status().isCreated())
                .andReturn();

        result = mockMvc
                .perform(get("/accounts/{accountId}", expectedAccount.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expectedAccount.getName()))
                .andExpect(jsonPath("$.fio").value(expectedAccount.getFio()))
                .andExpect(jsonPath("$.description").value(expectedAccount.getDescription()))
                .andExpect(jsonPath("$.isDelete").value(expectedAccount.getIsDelete()))
                //.andExpect(jsonPath("$.role.id").value(expectedRole.getId()))
                .andExpect(jsonPath("$.role.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.role.isAdmin").value(expectedRole.getIsAdmin()))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void updateAccount_Success() throws Exception {

        UUID roleId = UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F");

        DtoRole expectedRole = new DtoRole()
                .id(roleId)
                .name("Администратор")
                .isAdmin(true);

        DtoAccount expectedAccount = new DtoAccount()
                .id(UUID.fromString("a8d522eb-30f1-41c3-89e9-fdc6050f16aa"))
                .name("user2 имзененное имя")
                .fio("Петров П.П. измененное имя")
                .description("оператор измененное описание")
                .role(expectedRole)
                .isDelete(false);

        MvcResult result = mockMvc
                .perform(put("/accounts/{accountId}", expectedAccount.getId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expectedAccount)))
                .andExpect(status().isOk())
                .andReturn();


        result = mockMvc
                .perform(get("/accounts/{accountId}", expectedAccount.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expectedAccount.getName()))
                .andExpect(jsonPath("$.fio").value(expectedAccount.getFio()))
                .andExpect(jsonPath("$.description").value(expectedAccount.getDescription()))
                .andExpect(jsonPath("$.isDelete").value(expectedAccount.getIsDelete()))
                //.andExpect(jsonPath("$.role.id").value(expectedRole.getId()))
                .andExpect(jsonPath("$.role.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.role.isAdmin").value(expectedRole.getIsAdmin()))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void updateAccount_NotFound() throws Exception {

        UUID roleId = UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F");

        DtoRole expectedRole = new DtoRole()
                .id(roleId)
                .name("Администратор")
                .isAdmin(true);

        Map<UUID, DtoRole> roles = new HashMap<>();
        roles.put(roleId, expectedRole);

        DtoAccount expectedAccount = new DtoAccount()
                .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                .name("user2 имзененное имя")
                .fio("Петров П.П. измененное имя")
                .description("оператор измененное описание")
                .role(expectedRole)
                .isDelete(false);

        MvcResult result = mockMvc
                .perform(put("/accounts/{accountId}", expectedAccount.getId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expectedAccount)))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
