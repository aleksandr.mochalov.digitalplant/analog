package ru.digitalplant.analog.integration.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.digitalplant.analog.model.DtoRole;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class RoleAPIControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRoleById_Success() throws Exception{

        DtoRole expected = new DtoRole()
                .id(UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F"))
                .name("Администратор")
                .isAdmin(true);

        MvcResult result = mockMvc
                .perform(get("/roles/{roleId}", expected.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expected.getName()))
                .andExpect(jsonPath("$.isAdmin").value(expected.getIsAdmin()))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRoleById_NotFound() throws Exception{

        MvcResult result = mockMvc
                .perform(get("/roles/{roleId}", UUID.randomUUID()))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRoleById_BedRequest1() throws Exception{

        MvcResult result = mockMvc
                .perform(get("/roles/sdsdsdsdsds"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRoleById_BedRequest2() throws Exception{

        MvcResult result = mockMvc
                .perform(get("/roles/null"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRoles_Success() throws Exception{

        Collection<DtoRole> roles = new ArrayList<DtoRole>();
        roles.add( new DtoRole()
                .id(UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F"))
                .name("Администратор")
                .isAdmin(true)
        );

        roles.add( new DtoRole()
                .id(UUID.fromString("F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6"))
                .name("Пользователь")
                .isAdmin(false)
        );

        roles.add( new DtoRole()
                .id(UUID.fromString("a8d522eb-30f1-41c3-89e9-fdc6050f99cc"))
                .name("TestRole")
                .isAdmin(false)
        );

        MvcResult result = mockMvc
                .perform(get("/roles"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(roles)))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void addRole_Success() throws Exception{
        UUID id = UUID.randomUUID();
        DtoRole expected = new DtoRole()
                .id(id)
                .name("NewRole")
                .isAdmin(false);

        MvcResult result = mockMvc
                .perform(post("/roles")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isCreated())
                .andReturn();

         result = mockMvc
                .perform(get("/roles/{roleId}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expected.getName()))
                .andExpect(jsonPath("$.isAdmin").value(expected.getIsAdmin()))
                .andReturn();
    }

//    @Test
//    @Sql(scripts = "classpath:db/test-data.sql")
//    public void deleteRole_RoleExists_Success() throws Exception{
//        UUID id = UUID.fromString("a8d522eb-30f1-41c3-89e9-fdc6050f99cc");
//
//        MvcResult result = mockMvc
//                .perform(delete("/roles/{roleId}", id))
//                .andExpect(status().isOk())
//                .andReturn();
//
//         result = mockMvc
//                .perform(get("/roles/{roleId}", UUID.randomUUID()))
//                .andExpect(status().isNotFound())
//                .andReturn();
//    }

//    @Test
//    @Sql(scripts = "classpath:db/test-data.sql")
//    public void deleteRole_RoleNotExists_Success() throws Exception{
//        UUID id = UUID.randomUUID();
//
//        MvcResult result = mockMvc
//                .perform(delete("/roles/{roleId}", id))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        result = mockMvc
//                .perform(get("/roles/{roleId}", UUID.randomUUID()))
//                .andExpect(status().isNotFound())
//                .andReturn();
//    }
//
//    @Test
//    @Sql(scripts = "classpath:db/test-data.sql")
//    public void deleteRole_BedRequest1() throws Exception{
//
//        MvcResult result = mockMvc
//                .perform(delete("/roles/sdsdsdsdsds"))
//                .andExpect(status().isBadRequest())
//                .andReturn();
//    }
//
//    @Test
//    @Sql(scripts = "classpath:db/test-data.sql")
//    public void deleteRole_BedRequest2() throws Exception{
//
//        MvcResult result = mockMvc
//                .perform(delete("/roles/null"))
//                .andExpect(status().isBadRequest())
//                .andReturn();
//    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void updateRole_Succes() throws Exception{
        DtoRole expected = new DtoRole()
                .id(UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F"))
                .name("Администратор123")
                .isAdmin(false);

        MvcResult result = mockMvc
                .perform(put("/roles/{roleId}", expected.getId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isOk())
                .andReturn();

        result = mockMvc
                .perform(get("/roles/{roleId}", expected.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expected.getName()))
                .andExpect(jsonPath("$.isAdmin").value(expected.getIsAdmin()))
                .andReturn();
    }

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void updateRole_NotFound() throws Exception{
        DtoRole expected = new DtoRole()
                .id(UUID.randomUUID())
                .name("Администратор123")
                .isAdmin(false);

        MvcResult result = mockMvc
                .perform(put("/roles/{roleId}", expected.getId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isNotFound())
                .andReturn();

    }

}
