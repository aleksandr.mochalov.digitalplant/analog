package ru.digitalplant.analog.integration.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRequest;
import ru.digitalplant.analog.model.DtoRole;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestAPIControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper().setDateFormat(new StdDateFormat());

    @Test
    @Sql(scripts = "classpath:db/test-data.sql")
    public void getRequestById_Success() throws Exception{
        DtoRole expeactedRole = new DtoRole()
                .id(UUID.fromString("F60D6B50-47A9-4C69-87BF-8C1982D6413F"))
                .name("Администратор")
                .isAdmin(true);

        DtoAccount expectedAccount = new DtoAccount()
                .id(UUID.fromString("a8d522eb-30f1-41c3-89e9-fdc6050f15aa"))
                .name("user1")
                .fio("Иванов И.И.")
                .description(null)
                .isDelete(false)
                .role(expeactedRole);

        DtoRequest expectedRequest = new DtoRequest()
                .id(UUID.fromString("55000000-30f1-41c3-89e9-fdc6050f99a1"))
                .account(expectedAccount)
                .comment("comment1")
                .isApplay(false)
                .isDelete(false)
                .ts(Timestamp.valueOf("2019-03-01 9:01:00"));

        MvcResult result = mockMvc
                .perform(get("/requests/{requestId}", expectedRequest.getId())
                        .header("token", "1"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedRequest)))

//                .andExpect(jsonPath("$.id").value(expectedRequest.getId()))
//                .andExpect(jsonPath("$.account.id").value(expectedRequest.getAccount().getId()))

//                .andExpect(jsonPath("$.account.name").value(expectedRequest.getAccount().getName()))
//                .andExpect(jsonPath("$.account.fio").value(expectedRequest.getAccount().getFio()))
//                .andExpect(jsonPath("$.account.description").value(expectedRequest.getAccount().getDescription()))
//                .andExpect(jsonPath("$.account.isDelete").value(expectedRequest.getAccount().getIsDelete()))
//                .andExpect(jsonPath("$.account.role.name").value(expectedRequest.getAccount().getRole().getName()))
//                .andExpect(jsonPath("$.account.role.isAdmin").value(expectedRequest.getAccount().getRole().getIsAdmin()))
//                .andExpect(jsonPath("$.comment").value(expectedRequest.getComment()))
//                .andExpect(jsonPath("$.isApply").value(expectedRequest.getIsApply()))
//                .andExpect(jsonPath("$.isDelete").value(expectedRequest.getIsDelete()))

//                .andExpect(jsonPath("$.ts").value(expectedRequest.getTs()))
                .andReturn();
    }

}
