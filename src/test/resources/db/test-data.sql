--DELETE
DELETE FROM public.request;
DELETE FROM public.account;
DELETE FROM public.role;

--ROLE
INSERT INTO role( id, name, isadmin )
  SELECT 'F60D6B50-47A9-4C69-87BF-8C1982D6413F', 'Администратор', true
  WHERE NOT EXISTS (
    SELECT * FROM role r2 WHERE upper(r2.name) = upper('Администратор')
    );

INSERT INTO role( id, name, isadmin )
  SELECT 'F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6', 'Пользователь', false
  WHERE NOT EXISTS (
    SELECT * FROM role r2 WHERE upper(r2.name) = upper('Пользователь')
    );


INSERT INTO role( id, name, isadmin )
          VALUES('a8d522eb-30f1-41c3-89e9-fdc6050f99cc', 'TestRole', false );

--ACCOUNT
INSERT INTO public.account(
	id, name, fio, description, isdelete, idrole)
	VALUES ('a8d522eb-30f1-41c3-89e9-fdc6050f15aa', 'user1', 'Иванов И.И.', null, false, 'F60D6B50-47A9-4C69-87BF-8C1982D6413F'),
	        ('a8d522eb-30f1-41c3-89e9-fdc6050f16aa', 'user2', 'Петров П.П.', 'оператор', false, 'F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6'),
	        ('a8d522eb-30f1-41c3-89e9-fdc6050f17aa', 'user3', 'Сидоров С.С.', 'инженер', true, 'F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6');



--REQUEST
INSERT INTO public.request(
	id, idaccount, comment, isapply, isdelete, ts)
	VALUES       --for user1
	        ('55000000-30f1-41c3-89e9-fdc6050f99a1', 'a8d522eb-30f1-41c3-89e9-fdc6050f15aa', 'comment1', false, false, '2019-03-01 9:01:00'),
	        ('55000000-30f1-41c3-89e9-fdc6050f99a2', 'a8d522eb-30f1-41c3-89e9-fdc6050f15aa', 'comment2', true, false, '2019-03-01 10:00:00'),
	        ('55000000-30f1-41c3-89e9-fdc6050f99a3', 'a8d522eb-30f1-41c3-89e9-fdc6050f15aa', 'comment3', false, true, '2019-03-01 11:35:00'),

			     --for user2
	        ('60000000-30f1-41c3-89e9-fdc6050f99a1', 'a8d522eb-30f1-41c3-89e9-fdc6050f16aa', 'comment1', false, false, '2019-03-01 7:08:00'),
	        ('60000000-30f1-41c3-89e9-fdc6050f99a2', 'a8d522eb-30f1-41c3-89e9-fdc6050f16aa', 'comment2', true, false, '2019-03-01 10:15:00'),
	        ('60000000-30f1-41c3-89e9-fdc6050f99a3', 'a8d522eb-30f1-41c3-89e9-fdc6050f16aa', 'comment3', false, true, '2019-03-01 11:39:00'),

			     --for user3(пользователь удален)
	        ('70000000-30f1-41c3-89e9-fdc6050f99a1', 'a8d522eb-30f1-41c3-89e9-fdc6050f17aa', 'comment1', false, false, '2019-03-01 3:18:00'),
	        ('70000000-30f1-41c3-89e9-fdc6050f99a2', 'a8d522eb-30f1-41c3-89e9-fdc6050f17aa', 'comment2', true, false, '2019-03-01 12:11:00'),
	        ('70000000-30f1-41c3-89e9-fdc6050f99a3', 'a8d522eb-30f1-41c3-89e9-fdc6050f17aa', 'comment3', false, true, '2019-03-01 4:30:00');
