
CREATE TABLE role
(
    id uuid NOT NULL CONSTRAINT role_pk PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    isadmin boolean NOT NULL
);

CREATE TABLE account
(
    id uuid NOT NULL CONSTRAINT account_pk PRIMARY KEY,
    name VARCHAR(50) NOT NULL CHECK (name != ''),
    fio VARCHAR(100),
    description VARCHAR(200),
    isdelete boolean NOT NULL,
    idrole uuid NOT NULL,

    CONSTRAINT role_fk FOREIGN KEY (idrole)
        REFERENCES role (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE request
(
    id uuid NOT NULL CONSTRAINT request_pk PRIMARY KEY,
    idaccount uuid NOT NULL,
    comment text NOT NULL CHECK(comment != ''),
    isapply boolean NOT NULL,
    isdelete boolean NOT NULL,
    ts timestamp without time zone NOT NULL,
    CONSTRAINT account_fk FOREIGN KEY (idaccount)
        REFERENCES account (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO role( id, name, isadmin )
  SELECT 'F60D6B50-47A9-4C69-87BF-8C1982D6413F', 'Администратор', true
  WHERE NOT EXISTS (
    SELECT * FROM role r2 WHERE upper(r2.name) = upper('Администратор')
    );

INSERT INTO role( id, name, isadmin )
  SELECT 'F25B186D-DD6B-4DEB-BBC1-4A7465E0DAB6', 'Пользователь', false
  WHERE NOT EXISTS (
    SELECT * FROM role r2 WHERE upper(r2.name) = upper('Пользователь')
    );
