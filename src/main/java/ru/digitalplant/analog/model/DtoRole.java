package ru.digitalplant.analog.model;

import java.util.Collection;
import java.util.UUID;

public class DtoRole {

    private UUID id;

    private String name;

    private boolean isAdmin;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DtoRole id(UUID id){
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DtoRole name(String name){
        this.name = name;
        return this;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public DtoRole isAdmin(boolean isAdmin){
        this.isAdmin = isAdmin;
        return this;
    }
}
