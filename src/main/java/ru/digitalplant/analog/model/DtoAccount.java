package ru.digitalplant.analog.model;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DtoAccount {
    private UUID id;

    private String name;

    private String fio;

    private String description;

    private boolean isDelete;

    private DtoRole role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DtoAccount id(UUID id){
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DtoAccount name(String name){
        this.name = name;
        return this;
    }


    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public DtoAccount fio(String fio){
        this.fio = fio;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DtoAccount description(String description){
        this.description = description;
        return this;
    }

    public DtoRole getRole() {
        return role;
    }

    public void setRole(DtoRole role) {
        this.role = role;
    }

    public DtoAccount role(DtoRole role){
        this.role = role;
        return this;
    }

    public boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public DtoAccount isDelete(boolean isDelete){
        this.isDelete = isDelete;
        return this;
    }
}
