package ru.digitalplant.analog.model;

import java.sql.Timestamp;
import java.util.UUID;

public class DtoRequest {

    private UUID id;

    private DtoAccount account;

    private String comment;

    private boolean isApply;

    private boolean isDelete;

    private Timestamp ts;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DtoRequest id(UUID id){
        this.id = id;
        return this;
    }

    public DtoAccount getAccount() {
        return account;
    }

    public void setAccount(DtoAccount account) {
        this.account = account;
    }

    public DtoRequest account(DtoAccount account){
        this.account = account;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DtoRequest comment(String comment){
        this.comment = comment;
        return this;
    }

    public boolean getIsApply() {
        return isApply;
    }

    public void setIsApply(boolean apply) {
        isApply = apply;
    }

    public DtoRequest isApplay(boolean isApply){
        this.isApply = isApply;
        return this;
    }

    public boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean delete) {
        isDelete = delete;
    }

    public DtoRequest isDelete(boolean isDelete){
        this.isDelete = isDelete;
        return this;
    }

    public Timestamp getTs() {
        return ts;
    }

    public void setTs(Timestamp ts) {
        this.ts = ts;
    }

    public DtoRequest ts(Timestamp ts){
        this.ts = ts;
        return this;
    }
}
