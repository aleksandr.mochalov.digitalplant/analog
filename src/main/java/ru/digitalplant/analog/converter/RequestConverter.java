package ru.digitalplant.analog.converter;

import org.jooq.Record;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRequest;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Account;
import ru.digitalplant.analog.model.tables.Request;
import ru.digitalplant.analog.model.tables.Role;

public class RequestConverter {
    public static DtoRequest fromRecord(Record r) {
        DtoRequest request = new DtoRequest()
                .id(r.getValue(Request.REQUEST.ID))
                .account(AccountConverter.fromRecord(r))
                .comment(r.getValue(Request.REQUEST.COMMENT))
                .isApplay(r.getValue(Request.REQUEST.ISAPPLY))
                .isDelete(r.getValue(Request.REQUEST.ISDELETE))
                .ts(r.getValue(Request.REQUEST.TS));

        return request;
    }
}
