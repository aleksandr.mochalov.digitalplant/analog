package ru.digitalplant.analog.converter;

import org.jooq.Record;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Account;
import ru.digitalplant.analog.model.tables.Role;

public class AccountConverter {

    public static DtoAccount fromRecord(Record r) {
        DtoAccount account = new DtoAccount()
                .id(r.getValue(Account.ACCOUNT.ID))
                .name(r.getValue(Account.ACCOUNT.NAME))
                .fio(r.getValue(Account.ACCOUNT.FIO))
                .description(r.getValue(Account.ACCOUNT.DESCRIPTION))
                .isDelete(r.getValue(Account.ACCOUNT.ISDELETE))
                .role(RoleConverter.fromRecord(r));

        return account;
    }

}
