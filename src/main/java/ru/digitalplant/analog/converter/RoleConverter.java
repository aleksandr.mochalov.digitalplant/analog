package ru.digitalplant.analog.converter;

import org.jooq.Record;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Role;

public class RoleConverter {
    public static DtoRole fromRecord(Record r) {
        DtoRole role = new DtoRole()
                .id(r.getValue(Role.ROLE.ID))
                .name(r.getValue(Role.ROLE.NAME))
                .isAdmin(r.getValue(Role.ROLE.ISADMIN));

        return role;
    }
}
