package ru.digitalplant.analog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalogApplication.class, args);
	}

}
