package ru.digitalplant.analog.provider;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.digitalplant.analog.converter.AccountConverter;
import ru.digitalplant.analog.converter.RequestConverter;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRequest;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Account;
import ru.digitalplant.analog.model.tables.Request;
import ru.digitalplant.analog.model.tables.Role;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class RequestProviderImpl implements RequestProvider {

    private static final Logger log = LoggerFactory.getLogger(RoleProviderImpl.class);

    private DSLContext context;

    @Autowired
    public RequestProviderImpl(DSLContext context) {
        this.context = context;
    }

    @Override
    public List<DtoRequest> getRequests(Date  timeStart, Date timeEnd) {
        try {

            Timestamp tsStart = new Timestamp(timeStart.getTime());
            Timestamp tsEnd = new Timestamp(timeEnd.getTime());

            List<DtoRequest> requests = new ArrayList<>();
            context
                    .select()
                    .from(Request.REQUEST)
                    .innerJoin(Account.ACCOUNT)
                    .on(Request.REQUEST.IDACCOUNT.eq(Account.ACCOUNT.ID))
                    .leftJoin(Role.ROLE)
                    .on(Account.ACCOUNT.IDROLE.eq(Role.ROLE.ID))
                    .where(Request.REQUEST.ISDELETE.eq(false).and(Account.ACCOUNT.ISDELETE.eq(false)).and(Request.REQUEST.TS.between(tsStart, tsEnd)))
                    .fetch(record -> record.map(r -> requests.add(RequestConverter.fromRecord(r)) ));



            return requests;
        }
        catch (Exception ex){
            String msg = String.format("getRequests: %s", ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public DtoRequest getRequest(UUID id) {
        try {
            return context
                    .select()
                    .from(Request.REQUEST)
                    .innerJoin(Account.ACCOUNT)
                    .on(Request.REQUEST.IDACCOUNT.eq(Account.ACCOUNT.ID))
                    .leftJoin(Role.ROLE)
                    .on(Account.ACCOUNT.IDROLE.eq(Role.ROLE.ID))
                    .where(Request.REQUEST.ID.eq(id).and(Request.REQUEST.ISDELETE.eq(false)).and(Account.ACCOUNT.ISDELETE.eq(false)))
                    .fetchAny(record -> record.map(r -> RequestConverter.fromRecord(r) ));

        }
        catch (Exception ex){
            String msg = String.format("getRequest(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void addRequest(DtoRequest request) {
        try {
            if (request == null) throw new NullPointerException();

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf)
                                .insertInto(Request.REQUEST)
                                .columns(Request.REQUEST.ID, Request.REQUEST.IDACCOUNT, Request.REQUEST.COMMENT, Request.REQUEST.ISAPPLY, Request.REQUEST.ISDELETE, Request.REQUEST.TS)
                                .values(request.getId(), request.getAccount().getId(), request.getComment(), request.getIsApply(), request.getIsDelete(), request.getTs())
                                .execute();

                        if (rc != 1) throw new Exception();
                    }
                    );
        }
        catch (Exception ex){
            String msg = String.format("addRequest(%s): %s", request, ex.getMessage());
            log.error(msg, ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void updateRequest(UUID id, DtoRequest request) {
        try {
            if (request == null) throw new NullPointerException();
            if (getRequest(id) == null) throw new Exception(String.format("not found request (id=%s)", id));

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf).update(Request.REQUEST)
                                .set(Request.REQUEST.ISAPPLY, request.getIsApply())
                                .where(Request.REQUEST.ID.eq(id))
                                .execute();

                        if (rc != 1) throw new Exception();
                    }
            );
        }
        catch (Exception ex){
            String msg=String.format("updateRequest(%s): %s: %s", id, request, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void deleteRequest(UUID id) {
        try {
            if (getRequest(id) == null) throw new Exception(String.format("not found request (id=%s)", id));

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf).update(Request.REQUEST)
                                .set(Request.REQUEST.ISDELETE, true)
                                .where(Request.REQUEST.ID.eq(id))
                                .execute();

                        if (rc != 1) throw new Exception();
                    }
            );
        }
        catch (Exception ex){
            String msg=String.format("deleteRequest(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }
}
