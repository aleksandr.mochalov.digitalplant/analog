package ru.digitalplant.analog.provider;

import ru.digitalplant.analog.model.DtoRequest;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface RequestProvider {
    List<DtoRequest> getRequests(Date timeStart, Date timeEnd);
    DtoRequest getRequest(UUID id);
    void addRequest(DtoRequest request);
    void updateRequest(UUID id, DtoRequest request);
    void deleteRequest(UUID id);
}
