package ru.digitalplant.analog.provider;

import ru.digitalplant.analog.model.DtoRole;

import java.util.List;
import java.util.UUID;

public interface RoleProvider {
    DtoRole getRole(UUID id);
    List<DtoRole> getRoles();

    void addRole(DtoRole role);
    void updateRole(UUID id, DtoRole role);
    void deleteRole(UUID id);
}
