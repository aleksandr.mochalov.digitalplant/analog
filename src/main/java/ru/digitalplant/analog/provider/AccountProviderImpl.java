package ru.digitalplant.analog.provider;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.digitalplant.analog.converter.AccountConverter;
import ru.digitalplant.analog.converter.RoleConverter;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Account;
import ru.digitalplant.analog.model.tables.Role;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AccountProviderImpl implements AccountProvider {

    private static final Logger log = LoggerFactory.getLogger(RoleProviderImpl.class);

    private DSLContext context;

    @Autowired
    public AccountProviderImpl(DSLContext context) {
        this.context = context;
    }

    @Override
    public List<DtoAccount> getAccounts() {
        try {

            List<DtoAccount> accounts = new ArrayList<DtoAccount>();
            context
                    .select()
                    .from(Account.ACCOUNT)
                    .leftJoin(Role.ROLE)
                    .on(Account.ACCOUNT.IDROLE.eq(Role.ROLE.ID))
                    .where(Account.ACCOUNT.ISDELETE.eq(false))
                    .fetch(record -> record.map(r -> accounts.add(AccountConverter.fromRecord(r)) ));


            return accounts;
        }
        catch (Exception ex){
            String msg = String.format("getAccounts: %s", ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public DtoAccount getAccount(UUID id) {
        try {

            return context
                    .select()
                    .from(Account.ACCOUNT)
                    .leftJoin(Role.ROLE)
                    .on(Account.ACCOUNT.IDROLE.eq(Role.ROLE.ID))
                    .where(Account.ACCOUNT.ID.eq(id).and(Account.ACCOUNT.ISDELETE.eq(false)))
                    .fetchAny(record -> record.map(r -> AccountConverter.fromRecord(r)));



        }
        catch (Exception ex){
            String msg = String.format("getAccount(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void addAccount(DtoAccount account) {
        try {
            if (account == null) throw new NullPointerException();

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf)
                                .insertInto(Account.ACCOUNT)
                                .columns(Account.ACCOUNT.ID, Account.ACCOUNT.NAME, Account.ACCOUNT.FIO, Account.ACCOUNT.DESCRIPTION, Account.ACCOUNT.ISDELETE, Account.ACCOUNT.IDROLE)
                                .values(account.getId(), account.getName(), account.getFio(), account.getDescription(), account.getIsDelete(), account.getRole().getId())
                                .execute();

                        if (rc != 1) throw new Exception();

                    }
            );
        }
        catch (Exception ex) {
            String msg = String.format("addAccount(%s): %s", account, ex.getMessage());
            log.error(msg, ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void updateAccount(UUID id, DtoAccount account) {
        try {
            if (account == null) throw new NullPointerException();
            DtoAccount oldAccount = getAccount(id);
            if (oldAccount == null) throw new Exception(String.format("not found account (id=%s)", id));

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf)
                                .update(Account.ACCOUNT)
                                .set(Account.ACCOUNT.ID, account.getId())
                                .set(Account.ACCOUNT.NAME, account.getName())
                                .set(Account.ACCOUNT.FIO, account.getFio())
                                .set(Account.ACCOUNT.DESCRIPTION, account.getDescription())
                                .set(Account.ACCOUNT.ISDELETE, account.getIsDelete())
                                .set(Account.ACCOUNT.IDROLE, account.getRole().getId())
                                .where(Account.ACCOUNT.ID.eq(id))
                                .execute();

                        if (rc != 1) throw new Exception();
                    }
            );
        }
        catch (Exception ex) {
            String msg=String.format("updateAccount(%s): %s: %s", id, account, ex.getMessage());
            log.error(msg, ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void deleteAccount(UUID id) {
        try {
            if (getAccount(id) == null) throw new Exception(String.format("not found account (id=%s)", id));

            context.transaction(conf ->
                    {
                        int rc = DSL.using(conf).
                                update(Account.ACCOUNT)
                                .set(Account.ACCOUNT.ISDELETE, true)
                                .where(Account.ACCOUNT.ID.eq(id))
                                .execute();

                        if (rc != 1) throw new Exception();
                    }
            );
        }
        catch (Exception ex){
            String msg=String.format("deleteAccount(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }
}
