package ru.digitalplant.analog.provider;

import ru.digitalplant.analog.model.DtoAccount;

import java.util.List;
import java.util.UUID;

public interface AccountProvider {
    List<DtoAccount> getAccounts();
    DtoAccount getAccount(UUID id);
    void addAccount(DtoAccount account);
    void updateAccount(UUID id, DtoAccount account);
    void deleteAccount(UUID id);
}
