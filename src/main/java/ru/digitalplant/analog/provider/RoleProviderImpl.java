package ru.digitalplant.analog.provider;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.digitalplant.analog.converter.RoleConverter;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.model.tables.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class RoleProviderImpl implements RoleProvider {

    private static final Logger log = LoggerFactory.getLogger(RoleProviderImpl.class);

    private DSLContext context;

    @Autowired
    public RoleProviderImpl(DSLContext context) {
        this.context = context;
    }

    @Override
    public DtoRole getRole(UUID id) {

        try{
             return context
                    .select()
                    .from(Role.ROLE)
                    .where(Role.ROLE.ID.eq(id))
                    .fetchAny(r -> RoleConverter.fromRecord(r));

        }
        catch (Exception ex){
            String msg=String.format("getRole(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public List<DtoRole> getRoles() {
        try {
            List<DtoRole> roles = new ArrayList<DtoRole>();
            context
                    .select()
                    .from(Role.ROLE)
                    .fetch(record -> record.map(r -> roles.add(RoleConverter.fromRecord(r))));

            return roles;
        }
        catch (Exception ex){
            String msg=String.format("getRoles: %s", ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void addRole(DtoRole role) {
        try {
            if (role == null) throw new NullPointerException();

            context.transaction(conf ->
            {
                int rc = DSL.using(conf).insertInto(Role.ROLE)
                    .columns(Role.ROLE.ID, Role.ROLE.NAME, Role.ROLE.ISADMIN)
                    .values(role.getId(), role.getName(), role.getIsAdmin())
                    .execute();

                if (rc != 1) throw new Exception();
            }
            );
        }
        catch (Exception ex){
            String msg=String.format("addRole(%s): %s", role, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void updateRole(UUID id, DtoRole role) {
        try {
            if (role == null) throw new NullPointerException();
            if (getRole(id) == null) throw new Exception(String.format("not found role (id=%s)", id));

            context.transaction(conf ->
            {
                int rc = DSL.using(conf).update(Role.ROLE)
                        .set(Role.ROLE.NAME, role.getName())
                        .set(Role.ROLE.ISADMIN, role.getIsAdmin())
                        .where(Role.ROLE.ID.eq(id))
                        .execute();

                if (rc != 1) throw new Exception();
            }
            );
        }
        catch (Exception ex){
            String msg=String.format("updateRole(%s): %s: %s", id, role, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void deleteRole(UUID id) {
        try {
            context.transaction(conf ->
            {
                DSL.using(conf).delete(Role.ROLE)
                        .where(Role.ROLE.ID.eq(id))
                        .execute();
            }
            );
        }
        catch (Exception ex){
            String msg=String.format("deleteRole(%s): %s", id, ex.getMessage());
            log.error(msg,ex);
            throw new RuntimeException(msg);
        }
    }
}
