package ru.digitalplant.analog.api;

import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalplant.analog.model.DtoRole;
import ru.digitalplant.analog.provider.RoleProvider;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class RoleAPIImpl implements RoleAPI {

    private static final Logger log = LoggerFactory.getLogger(RoleAPIImpl.class);

    private final RoleProvider provider;

    @Autowired
    public RoleAPIImpl(RoleProvider provider) {
        this.provider = provider;
    }

    @Override
    public ResponseEntity<DtoRole> getRoleById(@PathVariable("id") UUID id) {
        try {
            DtoRole role = provider.getRole(id);
            if (role == null) {
                return new ResponseEntity<DtoRole>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<DtoRole>(role, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<DtoRole>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<DtoRole>> getRoles() {
        try {
            List<DtoRole> roles = provider.getRoles();
            return new ResponseEntity<List<DtoRole>>(roles, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<List<DtoRole>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> addRole(@Valid DtoRole dtoRole) {
        try {
            provider.addRole(dtoRole);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> updateRoleById(UUID id, DtoRole dtoRole) {
        try {
            if (provider.getRole(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

            provider.updateRole(id, dtoRole);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @Override
//    public ResponseEntity<Void> deleteRoleById(UUID id) {
//        try {
//            if (provider.getRole(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
//
//            provider.deleteRole(id);
//            return new ResponseEntity<Void>(HttpStatus.OK);
//        } catch (Exception e) {
//            log.error("Couldn't serialize response for content type application/json", e);
//            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

}
