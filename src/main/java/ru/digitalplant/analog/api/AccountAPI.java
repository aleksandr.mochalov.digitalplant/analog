package ru.digitalplant.analog.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.digitalplant.analog.model.DtoAccount;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

public interface AccountAPI {

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    ResponseEntity<List<DtoAccount>> getAccounts();

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.GET)
    ResponseEntity<DtoAccount> getAccountById(@PathVariable("id") UUID id);

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    ResponseEntity<Void> addAccount(@Valid @RequestBody DtoAccount dtoAccount);

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.PUT)
    ResponseEntity<Void> updateAccount(@PathVariable("id") UUID id, @Valid @RequestBody DtoAccount dtoAccount);

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteAccount(@PathVariable("id") UUID id);
}
