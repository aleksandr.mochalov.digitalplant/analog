package ru.digitalplant.analog.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.digitalplant.analog.model.DtoRole;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

public interface RoleAPI {

    @RequestMapping(value = "/roles/{id}", method = RequestMethod.GET)
    ResponseEntity<DtoRole> getRoleById(@PathVariable("id") UUID id);

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    ResponseEntity<List<DtoRole>> getRoles();

    @RequestMapping(value = "/roles", method = RequestMethod.POST)
    ResponseEntity<Void> addRole(@Valid @RequestBody DtoRole dtoRole);

    @RequestMapping(value = "/roles/{id}", method = RequestMethod.PUT)
    ResponseEntity<Void> updateRoleById(@PathVariable("id") UUID id, @Valid @RequestBody DtoRole dtoRole);

//    @RequestMapping(value = "/roles/{id}", method = RequestMethod.DELETE)
//    ResponseEntity<Void> deleteRoleById(@PathVariable("id") UUID id);
}
