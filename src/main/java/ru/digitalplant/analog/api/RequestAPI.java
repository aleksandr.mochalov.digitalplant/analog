package ru.digitalplant.analog.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRequest;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface RequestAPI {
    @RequestMapping(value = "/requests/{id}", method = RequestMethod.GET)
    ResponseEntity<DtoRequest> getRequestById(@RequestHeader(value="token", required=true) String token, @PathVariable("id") UUID id);

    @RequestMapping(value = "/requests", method = RequestMethod.GET)
    ResponseEntity<List<DtoRequest>> getRequests(@RequestHeader(value="token", required=true) String token, Date timeStart, Date timeEnd, @Valid @RequestBody List<UUID> accountIds);

    @RequestMapping(value = "/requests", method = RequestMethod.POST)
    ResponseEntity<Void> addRequest(@RequestHeader(value="token", required=true) String token, @Valid @RequestBody DtoRequest dtoRequest);

    @RequestMapping(value = "/requests/{id}", method = RequestMethod.PUT)
    ResponseEntity<Void> updateRequestById(@RequestHeader(value="token", required=true) String token, @PathVariable("id") UUID id, @Valid @RequestBody DtoRequest dtoRequest);

    @RequestMapping(value = "/requests/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteRequestById(@RequestHeader(value="token", required=true) String token, @PathVariable("id") UUID id);
}
