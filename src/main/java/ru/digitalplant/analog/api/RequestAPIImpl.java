package ru.digitalplant.analog.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.model.DtoRequest;
import ru.digitalplant.analog.provider.AccountProvider;
import ru.digitalplant.analog.provider.RequestProvider;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class RequestAPIImpl implements RequestAPI {

    private static final Logger log = LoggerFactory.getLogger(RoleAPIImpl.class);

    private final RequestProvider provider;

    @Autowired
    public RequestAPIImpl(RequestProvider provider) {
        this.provider = provider;
    }

    @Override
    public ResponseEntity<DtoRequest> getRequestById(@RequestHeader(value="token", required=true) String token, UUID id) {
        try {
            if (!checkToken(token)){
                log.error("getRequestById UNAUTHORIZED");
                return new ResponseEntity<DtoRequest>(HttpStatus.UNAUTHORIZED);
            }

            DtoRequest request = provider.getRequest(id);
            if (request == null) {
                return new ResponseEntity<DtoRequest>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<DtoRequest>(request, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<DtoRequest>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<DtoRequest>> getRequests(@RequestHeader(value="token", required=true) String token, Date  timeStart, Date timeEnd, @Valid List<UUID> accountIds) {
        try {
            if (!checkToken(token)){
                log.error("getRequests UNAUTHORIZED");
                return new ResponseEntity<List<DtoRequest>>(HttpStatus.UNAUTHORIZED);
            }

            List<DtoRequest> requests = provider.getRequests(timeStart, timeEnd);
            return new ResponseEntity<List<DtoRequest>>(requests, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<List<DtoRequest>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> addRequest(@RequestHeader(value="token", required=true) String token, @Valid DtoRequest dtoRequest) {
        try {
            if (!checkToken(token)){
                log.error("addRequest UNAUTHORIZED");
                return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
            }

            provider.addRequest(dtoRequest);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> updateRequestById(@RequestHeader(value="token", required=true) String token, UUID id, @Valid DtoRequest dtoRequest) {
        try {
            if (!checkToken(token)){
                log.error("updateRequestById UNAUTHORIZED");
                return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
            }

            if (provider.getRequest(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            provider.updateRequest(id, dtoRequest);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> deleteRequestById(@RequestHeader(value="token", required=true) String token, UUID id) {
        try {
            if (!checkToken(token)){
                log.error("deleteRequestById UNAUTHORIZED");
                return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
            }

            if (provider.getRequest(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            provider.deleteRequest(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean checkToken(String token){
        return token == "1";
        //return true;
    }
}
