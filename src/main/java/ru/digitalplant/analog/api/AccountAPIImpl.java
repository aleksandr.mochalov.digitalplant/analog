package ru.digitalplant.analog.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalplant.analog.model.DtoAccount;
import ru.digitalplant.analog.provider.AccountProvider;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class AccountAPIImpl implements AccountAPI {

    private static final Logger log = LoggerFactory.getLogger(RoleAPIImpl.class);

    private final AccountProvider provider;

    @Autowired
    public AccountAPIImpl(AccountProvider provider) {
        this.provider = provider;
    }

    @Override
    public ResponseEntity<List<DtoAccount>> getAccounts() {
        try {
            List<DtoAccount> accounts = provider.getAccounts();
            return new ResponseEntity<List<DtoAccount>>(accounts, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<List<DtoAccount>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<DtoAccount> getAccountById(UUID id) {
        try {
            DtoAccount account = provider.getAccount(id);
            if (account == null) {
                return new ResponseEntity<DtoAccount>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<DtoAccount>(account, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<DtoAccount>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> addAccount(@Valid DtoAccount dtoAccount) {
        try {
            provider.addAccount(dtoAccount);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> updateAccount(UUID id, @Valid DtoAccount dtoAccount) {
        try {
            if (provider.getAccount(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            provider.updateAccount(id, dtoAccount);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> deleteAccount(UUID id) {
        try {
            if (provider.getAccount(id) == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            provider.deleteAccount(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
